# CoreMedia - solr_cores

Deploy CoreMedia specific solr cores and there configurations.

### config

```
---

coremedia_application_name: solr


solr:

  home: /opt/coremedia/solr-home
  data_directory: /var/coremedia/solr-data

  user: solr
  group: coremedia

  configsets:
    - cae
    - content
    - elastic

  solr_cores:
    live:
      config_set: cae
    preview:
      config_set: cae
    content:
      config_set: content

  clean_solr_home_on_update: true

  config_zip:
    group_id: com.coremedia.blueprint
    artifact_id: solr-config
    checksum: 3a4f376181c56053d0ad8ec54fd86a5f7f524b3d
```
