
import pytest
import os
import yaml
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture()
def AnsibleDefaults():
    with open("../../defaults/main.yml", 'r') as stream:
        return yaml.load(stream)


@pytest.mark.parametrize("dirs", [
    "/tmp/deployment_artefacts",
    "/opt/solr",
    "/var/log/solr",
    "/run/solr",
    "/opt/coremedia/solr-home",
    "/var/coremedia/solr-data"
])
def test_directories(host, dirs):
    d = host.file(dirs)
    assert d.is_directory
    assert d.exists


@pytest.mark.parametrize("files", [
    "/etc/default/solr.in.sh",
    "/opt/solr/bin/solr",
    "/opt/solr/bin/solr.in.sh"
])
def test_files(host, files):
    f = host.file(files)
    assert f.exists
    assert f.is_file


def test_user(host):
    assert host.group("coremedia").exists
    assert host.user("solr").exists
    assert host.user("solr").shell == "/bin/bash"


def test_config(host):
    config_file = "/etc/default/solr.in.sh"
    content = host.file(config_file).content_string

    assert 'SOLR_LOGS_DIR="/var/log/solr"' in content
    assert 'SOLR_HOME="/opt/coremedia/solr-home"' in content
    assert 'ENABLE_REMOTE_JMX_OPTS="true"' in content


def test_service(host):
    service = host.service("solr")
    assert service.is_enabled
    assert service.is_running


@pytest.mark.parametrize("ports", [
    '127.0.0.1:39080',
    '127.0.0.1:40080',
])
def test_open_port(host, ports):

    for i in host.socket.get_listening_sockets():
        print( i )

    application = host.socket("tcp://{}".format(ports))
    assert application.is_listening

